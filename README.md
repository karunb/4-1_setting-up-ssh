#SETTING UP SSH (secure shell)

# Team-Members

1. Karun Bourishetty
2. Sambi Reddy Chanimella
3. Sai Prakash Reddy Mamidi
4. Razeena Rao Nemarugommula
5. Anuraag Naidu Sanakkayala

## softwares needed

1. Git-bash
2. PuttyGen (download here http://www.puttygen.com/)

# account in bitbucket or github

#step to follow in order to set up SSH.

1. we generate RSA public and private key using PUTTY-Gen.
2. Enter a new passphrase which is of your choice, reenter the same in the next input.(which will be used while pull or pushing the repo).
3. save the private key in C:\Users\(your_systemname)\.ssh (if there isnt any folder create a new folder using touch command in gitbash).
4. login to your bitbucket --> direct to settings --> in the menu under security, click on ssh.
5. you can find add key button, click on it and enter the label as required and paste the public key which you have generated using puttyGen
6. when pushing the repo into the cloud or while pulling the repo, in the dialogue box we can see an input field called putty key or load putty
key(browse through the fileexplorer and select the key which you have created).
7. After we click on, we get a dialogue box, where it asks for the passphrase, enter the same where you filled in puttygen.
8. All done .. !!!! , the server will not ask for login details instead a passphrase is been asked.

#reference

https://confluence.atlassian.com/bitbucket/ssh-keys-935365775.html
https://help.github.com/articles/connecting-to-github-with-ssh/





